package com.api.gatway;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@GetMapping(value="/")
	public ResponseEntity<?> index() {
		Map<String, String> map = new HashMap<>();
		map.put("welcome", "Welcome to Api Gateway");
		return ResponseEntity.ok(map);
	}
}
