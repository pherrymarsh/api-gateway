FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/api-gatway-0.0.1-SNAPSHOT.jar app.jar
ADD cacert.pem cacert.pem
ARG DB_URL
ARG DB_USERNAME
ARG DB_PASS 
RUN keytool -import -trustcacerts -file cacert.pem -alias cacert -keystore $JAVA_HOME/jre/lib/security/cacerts  -storepass changeit -noprompt
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.datasource.url=$DB_URL","-Dspring.datasource.username=$DB_USERNAME","-Dspring.datasource.password=$DB_PASS","-jar","/app.jar"]